 //alert("B204!");

 // Using the exponent operator
 const firstNum = 8**3;
 console.log(firstNum);

 // Before the ES6 updates
 /*
	Syntax:
		Math.pow(base, exponent);
 */

 const secondNum = Math.pow(8,3);
 console.log(secondNum);


// Template Literals
/*
	Allows to write string without using the concatenation operator (+)
	${} - placeholder when using template literals
*/

let name = "John";

// Pre-Template Literal String
// Using single quote ('')
let message = 'Hello' + name + '! Welcome to programming!';
console.log("Message without template literals: " + message);

// Strings using Template Literals
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

// Multi-line using Template Literals
const anotherMessage = `
${name} attended a match competition.
He won it by solving the problem 8 ** 3 with the solution of ${firstNum}.`
console.log(anotherMessage);

let anotherMessage1 = '\n' + name + 'attended a match competition.' + '\n He won it by solving the problem 8 ** 3 with the solution of ' + firstNum;
console.log(anotherMessage1);

const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings is: ${principal * interestRate}`);

// Array Destructuring
/*
	Allows us to unpack elements in arrays into distinct variables.
	Allows us to name array elements with variables instead of using index numbers.
	
	Syntax:
		let/const [variableName1, variableName2, variableName3] = array

*/

const fullName = ["Joe", "Dela", "Cruz"];

// Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// Array Destructuring
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}`);

// Object Destructuring
/*
	Allows to unpack properties of objects into distinct variables
	Shortens the syntax for accessing properties from objects

	Syntax:
		let/const {propertyName, propertyName} = object

*/
const person = {
	givenName: "Raf",
	maidenName: "Vien",
	familyName: "Santillan"
};

// Pre-Object Destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

// Object Destructuring
const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

// function getFullName({givenName, maidenName, familyName}) {
// 	console.log(`${givenName} ${maidenName} ${familyName}`);
// }

function getFullName(parameter) {
	console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);

// Arrow Functions

/*
	-Compact alternative syntax to traditional functions
	Syntax:
		let/const variableNAme = () => {
			console.log();
		}
*/

const hello = () => {
	console.log("Hello World");
}

hello();

function hello1() {
	console.log("Hello World");
}

hello1();

// Pre-Arrow Function and Template Literals
/*
	Syntax:
		function functionName(parameterA, parameterB){
			console.log();
		}
*/

// function printFullName(firstName, middleInitial, lastName) {
// 	console.log(firstName + " " + middleInitial + ". " + lastName);
// }
// printFullName("Michael", "P", "Calasin");

printFullName = (firstName, middleInitial, lastName) =>  console.log(firstName + " " + middleInitial + ". " + lastName);

printFullName("Mikki Axel", "G", "Dela Cerna");

// Arrow function using Array Iteration Method
// Pre-arrow function
const student = ["Aron", "Daphne", "Edvic", "Angelo", "Roxanne"];

student.forEach(function(sutdent) {
	console.log(student + " is a student.");
});

// Arrow Function
student.forEach((student) => console.log(`${student} is a student.`));
student.forEach((student) => {
		console.log(`${student} is a student.`)
	});


// Implicit return statement
// Pre-arrow Function
function add(x,y) {
	return x+y;
}

let total = add(1,2);
console.log(add(100,200));

//Arrow Function
const add1 = (x,y) => x+y;
let total1 = add1(1,2);
console.log(total1);

console.log(add1(100, 500));


const add2 = (x,y) => {
	return x+y
}

let totalAdd = add2(1,1);
console.log(totalAdd);

// Default Function Argumaent Value
const greet = (name = 'User') => {
	return `Good morning, ${name}!`
}
console.log(greet());
console.log(greet("Mikki"));


function greet1 (name = "User", age = 18) {
	return `Good morning, ${name} Age: ${age}`;
}

console.log(greet1());
console.log(greet1("Mikki", 17));

// Class-Based Object Blueprint
/*
	Allow creation/instantiation of object using classes as blueprints.

	Syntax:
		class className {
			constructor(objectPropertyA, objectPropertyB) {
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
*/

class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

/*
	function Car (brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
*/

const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

const myNewCar = new Car("Ford", "Ranger Raptor", 2022);
console.log(myNewCar);